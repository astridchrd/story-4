
from django.urls import path
from . import views
from .views import schedule, schedule_create,deleteall
from profil import views

urlpatterns = [
    path('schedule/', schedule, name='schedule'),
    path('schedule/create', schedule_create, name='schedule_create'),
    path('schedule/delete', deleteall, name='deleteall'),
    path('', views.index, name="index"),
    path('about', views.about, name="about"),
    path('contact', views.about, name="contact"),
    path('gallery', views.about, name="gallery"),
]
