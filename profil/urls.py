from django.urls import path
from .views import index, contact, about, gallery

urlpatterns = [
    path('', index),
    path('about', about),
    path('gallery', gallery),
    path('contact', contact),
]